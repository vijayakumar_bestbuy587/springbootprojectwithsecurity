package com.vijay.SpringBootProjectWithSecurity.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


public class ConsumerKafka {
	
	//this is the annotation that is used to listen to kafka messages coming in
	//without this the messages cannot be read
//	@KafkaListener(groupId="${group.id}",topics= {"${kafka.topic}"}, id="consumer" )
//	public void onMessage(ConsumerRecord<String, Object> consumerRecord) {
//		 System.out.println("Consumed in kafka"+consumerRecord);
//	}
}
