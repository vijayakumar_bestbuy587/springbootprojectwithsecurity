package com.vijay.SpringBootProjectWithSecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;


/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
//Already tested with excluding the Jackson libs and serde works fine
//now to convert to xml and vice versa we need this. So not excluding
//@EnableAutoConfiguration(exclude = { JacksonAutoConfiguration.class })
@ComponentScan(basePackages="com.vijay.SpringBootProjectWithSecurity.*")
public class SpringBootSecurityApp {
	
	@Autowired
	Environment env;
	
    public static void main(String[] args) throws Exception {
    	System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "QA");
		 SpringApplication.run(SpringBootSecurityApp.class, args);	
		 
	}
	
}
