package com.vijay.SpringBootProjectWithSecurity.postprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

public class TestBeanFactoryPostProcessor implements ApplicationContextAware, BeanFactoryPostProcessor{
	
	private ApplicationContext appContext;
	
	

	
	
	public ApplicationContext getAppContext() {
		return this.appContext;
	}





	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				//get configurable env from context
				
				//setting the profile not working here
				ConfigurableEnvironment conf= (ConfigurableEnvironment) getAppContext().getEnvironment();
				//conf.setActiveProfiles("QA");
				//we can also set the active profile here but it is not a proper place to set
				//so set the profile in main app when it is running
				
				//we can get any bean metadata from the beanfactory and do the necessary changes
				//we can either set the env Lcp as QA heere and use Propert
				ApplicationHome home = new ApplicationHome(this.getClass());
				File dir = home.getDir();
				System.out.println("dfs"+dir);
				
				
				Environment env1 =  getAppContext().getEnvironment();
				System.out.println("Env here:"+env1.getProperty("LCP"));
				System.out.println("System.getProperty(\"user.dir\")"+System.getProperty("user.dir"));
				
				Properties prop = new Properties();
				try{
					FileReader fis = new FileReader(new File(dir+"/application_QA.properties"));
					prop.load(fis);
					fis.close();
					System.out.println(prop.getProperty("spring.datasource.driver-class-name"));
				}catch(FileNotFoundException e) {
					System.out.println("File not found");
				}catch(IOException e ){
					System.out.println("IO exception");
				}
				
				
				
				PropertyPlaceholderConfigurer cfg = new PropertyPlaceholderConfigurer();
				
				ClassPathResource cls = new ClassPathResource("classpath:/application_QA.properties");
				
				cfg.setLocation(cls);

				System.out.println(cfg);
				//now by default all the properties will be set in the environment
				//we can decide the file to choose based on $ and we can get the env using 
				//autowired properly and set the properties accordingly
		
	}





	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		// TODO Auto-generated method stub
		this.appContext = applicationContext;
	}
	

}
