package com.vijay.SpringBootProjectWithSecurity.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@XmlRootElement(name="User")
@Entity
@Table(name="users")
public class UserDto implements Serializable{
	@Id
	//@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="id")
	private int Id;
	
	@Column(name="First_Name")
	private String firstName;
	
	@Column(name="Last_Name")
	//@XmlElement(name="lastName")
	private String lastName;
	
	public UserDto() {
	}
	
	public UserDto(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
	}
	
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	
}
