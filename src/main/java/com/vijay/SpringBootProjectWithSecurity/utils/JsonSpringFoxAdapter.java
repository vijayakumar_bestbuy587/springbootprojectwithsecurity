package com.vijay.SpringBootProjectWithSecurity.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import springfox.documentation.spring.web.json.Json;

public class JsonSpringFoxAdapter extends CustomizedFactoryAdapter<Json>{

	public JsonSpringFoxAdapter() {
		super(Json.class);
		// TODO Auto-generated constructor stub
	}
	
	@Override protected void beforeWrite(Json source, JsonElement toSerialize) {
	    JsonObject custom = toSerialize.getAsJsonObject().get("custom").getAsJsonObject();
	    custom.add("size", new JsonPrimitive(custom.entrySet().size()));
	  }

	  @Override protected void afterRead(JsonElement deserialized) {
	    JsonObject custom = deserialized.getAsJsonObject().get("custom").getAsJsonObject();
	    custom.remove("size");
	  }
	
}
