package com.vijay.SpringBootProjectWithSecurity.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

//@ControllerAdvice(annotations=RestController.class)
public class RestAPIExceptionHandler  {
	
	
//	@ExceptionHandler(value= { HttpMediaTypeNotAcceptableException.class, HttpMediaTypeNotSupportedException.class})
//	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(RuntimeException ex,
//			HttpHeaders headers, HttpStatus status, WebRequest request) {
//		ErrorObject errObj = new ErrorObject(405, status, "Please enter a valid media type in the header");
//		return new ResponseEntity<>(errObj, headers, errObj.getHttpStatus());
//	}
//	
//	@ExceptionHandler(value= { InvalidPollException.class})
//	public ResponseEntity<ErrorObject> handleInvalidPollException(InvalidPollException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
//		System.out.println("Status returned when invalid exception is being thrown");
//		ErrorObject errObj = new ErrorObject(421, HttpStatus.BAD_REQUEST, ex.getLocalizedMessage());
//		return new ResponseEntity<ErrorObject>(errObj, headers, errObj.getHttpStatus());
//	}
//	
//	//this is to handle no exception resolver  or no exception handler found
//	@ExceptionHandler(value= {Exception.class})
//	protected ResponseEntity<Object> handleNoHandlerFoundException(
//	  NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
//	    String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();
//	    ErrorObject errObj = new ErrorObject(422, HttpStatus.NOT_FOUND, error);
//	    return new ResponseEntity<Object>(errObj, new HttpHeaders(), errObj.getHttpStatus());
//	}

}
