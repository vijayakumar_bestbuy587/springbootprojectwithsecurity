package com.vijay.SpringBootProjectWithSecurity.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.vijay.SpringBootProjectWithSecurity.postprocessor.TestBeanFactoryPostProcessor;
import com.vijay.SpringBootProjectWithSecurity.serviceImpl.PollJdbcSericeImpl;
import com.vijay.SpringBootProjectWithSecurity.serviceImpl.UserJdbcServiceImpl;

@Configuration
public class BeanConfiguration {

	@Bean
	public static TestBeanFactoryPostProcessor getTestBeanFactoryPostProcessorBean() {
		return new TestBeanFactoryPostProcessor();
	}
	
	@Bean
	public static PollJdbcSericeImpl getPollJdbcServiceImpl() {
		return new PollJdbcSericeImpl();
	}
	
	@Bean
	public static UserJdbcServiceImpl getUserServiceImpl() {
		return new UserJdbcServiceImpl();
	}
}
