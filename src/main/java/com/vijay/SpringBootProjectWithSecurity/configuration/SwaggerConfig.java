package com.vijay.SpringBootProjectWithSecurity.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {
	

//    @Bean
//    public Docket api() {
//    	
//    	ResponseMessageBuilder responseMessageBuider = new ResponseMessageBuilder();
//    	responseMessageBuider.code(500).message("Error message").responseModel(new ModelRef("ErrorObject"));
//    	ResponseMessage respMessage = responseMessageBuider.build();
//    	List<ResponseMessage> respMsgList = new ArrayList<>();
//    	respMsgList.add(respMessage);
//        return new Docket(DocumentationType.SWAGGER_2).select()
//        		.apis(RequestHandlerSelectors.basePackage("com.vijay.SpringBootProject.controllers"))
//        		.paths(PathSelectors.ant("/SpringBootSecurityApp/*")).build().apiInfo(apiInfo()).useDefaultResponseMessages(false);
//        //.globalResponseMessage(RequestMethod.GET,respMsgList);
//               //.globalResponseMessage(RequestMethod.GET,getDefautlt
//               //.responseModel(new ModelRef("Error")).build(), new ResponseMessageBuilder().code(403).message("Forbidden!!!!!").build()));
//    }
	  @Bean
	    public Docket api() {
	        return new Docket(DocumentationType.SWAGGER_2)
	                .apiInfo(getApiInfo())
	                .select()
	                .apis(RequestHandlerSelectors.basePackage("com.vijay.SpringBootProjectWithSecurity.controllers"))
	                .paths(PathSelectors.any()).build();
	    }

	    private ApiInfo getApiInfo() {
	        Contact contact = new Contact("Vijayakumar Shanmugam", "http://www.example.com", "A1394600@bestbuy.com");
	        return new ApiInfoBuilder()
	                .title("Example Api Title")
	                .description("Example Api Definition")
	                .version("1.0.0")
	                .license("Apache 2.0")
	                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
	                .contact(contact)
	                .build();
	    }

	
//    @Bean
//    public SecurityConfiguration security() {
//        return SecurityConfigurationBuilder.builder()
//            .clientId(CLIENT_ID)
//            .clientSecret(CLIENT_SECRET)
//            .scopeSeparator(" ")
//            .useBasicAuthenticationWithAccessCodeGrant(true)
//            .build();
//    }
    
//    private SecurityScheme securityScheme() {
//        GrantType grantType = new AuthorizationCodeGrantBuilder()
//            .tokenEndpoint(new TokenEndpoint(AUTH_SERVER + "/token", "oauthtoken"))
//            .tokenRequestEndpoint(
//              new TokenRequestEndpoint(AUTH_SERVER + "/authorize", CLIENT_ID, CLIENT_ID))
//            .build();
//     
//        SecurityScheme oauth = new OAuthBuilder().name("spring_oauth")
//            .grantTypes(Arrays.asList(grantType))
//            .scopes(Arrays.asList(scopes()))
//            .build();
//        return oauth;
//    }
    
//    private AuthorizationScope[] scopes() {
//        AuthorizationScope[] scopes = { 
//          new AuthorizationScope("read", "for read operations"), 
//          new AuthorizationScope("write", "for write operations"), 
//          new AuthorizationScope("foo", "Access foo API") };
//        return scopes;
//    }
//    
//    private SecurityContext securityContext() {
//        return SecurityContext.builder()
//          .securityReferences(Arrays.asList(new SecurityReference("spring_oauth", scopes())))
//          .forPaths(PathSelectors.regex("/SpringBootSecurityApp/*"))
//          .build();
//    }
		public void addResourceHandlers(ResourceHandlerRegistry registry) {
		    registry.addResourceHandler("swagger-ui.html")
		            .addResourceLocations("classpath:/META-INF/resources/");

		    registry.addResourceHandler("/webjars/**")
		            .addResourceLocations("classpath:/META-INF/resources/webjars/");
		}   
		

}
