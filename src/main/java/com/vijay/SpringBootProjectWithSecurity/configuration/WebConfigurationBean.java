package com.vijay.SpringBootProjectWithSecurity.configuration;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapterFactory;
import com.vijay.SpringBootProjectWithSecurity.utils.JsonSpringFoxAdapter;
import com.vijay.SpringBootProjectWithSecurity.utils.SpringFoxGsonAdapter;

import springfox.documentation.spring.web.json.Json;

public class WebConfigurationBean {
	
	
//	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//		//converters.add(createXstreamHttpMessageConverter());
//		converters.add(getGsonMessageConverter());
//		converters.add(getJacksonConverter());
//		super.configureMessageConverters(converters);
//		
//	}

	
//	@Bean
//	public MappingJackson2HttpMessageConverter getJacksonConverter() {
//		MappingJackson2HttpMessageConverter msgConverter = new MappingJackson2HttpMessageConverter();
//		ObjectMapper objMapper = new ObjectMapper();
//		msgConverter.setObjectMapper(objMapper);
//		return msgConverter;
//	}
	
	
//	 private HttpMessageConverter<Object> createXstreamHttpMessageConverter() {
//	        MarshallingHttpMessageConverter xstreamConverter = 
//	          new MarshallingHttpMessageConverter();
//	 
//	        XStreamMarshaller xstreamMarshaller = new XStreamMarshaller();
//	       
//	        xstreamConverter.setMarshaller(xstreamMarshaller);
//	        xstreamConverter.setUnmarshaller(xstreamMarshaller);
//	 
//	        return xstreamConverter;
//	    }
	
//	public GsonHttpMessageConverter getGsonMessageConverter(){
//		
//		GsonHttpMessageConverter gsonConverter = new GsonHttpMessageConverter();
//		GsonBuilder gsonBuilder = new GsonBuilder();
//
//		JsonSerializer<Json> springFoxSerializer = new JsonSerializer<Json>() {
//
//			@Override
//			public JsonElement serialize(Json src, Type typeOfSrc, JsonSerializationContext context) {
//				JsonParser jsonParser = new JsonParser();
//				return jsonParser.parse(src.value());
//			}
//			
//		};
//		gsonBuilder.registerTypeAdapter(Json.class, springFoxSerializer);
//		gsonBuilder.setPrettyPrinting();
//		Gson gson = gsonBuilder.create();
//		gsonConverter.setGson(gson);
//		return gsonConverter;
//		
//	}
	
//	@Override
//	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//		configurer.ignoreAcceptHeader(false);
//		Map<String, MediaType> mediaTypeMap = new HashMap<String, MediaType>();
//		mediaTypeMap.put("application/json", MediaType.APPLICATION_JSON_UTF8);
//		mediaTypeMap.put("application/xml", MediaType.APPLICATION_XML);
//		mediaTypeMap.put("text/xml", MediaType.APPLICATION_XML);
//		configurer
//		.mediaTypes(mediaTypeMap);
//	}
	
	
}
