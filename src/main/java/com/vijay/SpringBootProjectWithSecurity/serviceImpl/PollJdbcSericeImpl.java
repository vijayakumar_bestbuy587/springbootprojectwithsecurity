package com.vijay.SpringBootProjectWithSecurity.serviceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.vijay.SpringBootProjectWithSecurity.dto.Poll;

public class PollJdbcSericeImpl {
	
	@Autowired
	NamedParameterJdbcTemplate namedJdbcTemplate;
	
//	public List<Poll> getPolls(Integer id){
//		
//		String query = "SELECT * FROM POLL WHERE POLL_ID = :a";
//		SqlParameterSource paramSource = new MapSqlParameterSource("a", id);
//		return this.namedJdbcTemplate.query(query, paramSource, new PollMapper());
//	}
//	
//	//Rowmapper fr Poll
//	private static final class  PollMapper implements RowMapper<Poll> {
//
//		@Override
//		public Poll mapRow(ResultSet rs, int rowNum) throws SQLException {
//			Poll poll = new Poll();
//			poll.setId(rs.getInt("Id"));
//			poll.setPoll_count(rs.getInt("Poll_Count"));
//			poll.setPoll_name(rs.getString("Poll_Name"));
//			return poll;
//		}
//		
//	}
}
